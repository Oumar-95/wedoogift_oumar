import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AmountService {

    constructor(private httpClient: HttpClient){}
    checkAvailableAmount(amount:number){
        this.httpClient.post('http://localhost:3000/shop/5/search-combination/amount.json', amount).subscribe(
            () => {
                console.log('Enregistrement terminé');
            },
            (error) => {
                console.log('Erreur !: '+error);
            }
        );
    }
}
import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { AmountService } from '../services/amount.service';

@Component({
  selector: 'app-desired-amount',
  templateUrl: './desired-amount.component.html',
  styleUrls: ['./desired-amount.component.css']
})
export class DesiredAmountComponent implements OnInit {

  constructor(private amouuntService: AmountService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    this.amouuntService.checkAvailableAmount(form.value['amount']);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DesiredAmountComponent } from './desired-amount/desired-amount.component';
import { FormsModule }   from '@angular/forms';
import {AmountService} from './services/amount.service';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    DesiredAmountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AmountService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

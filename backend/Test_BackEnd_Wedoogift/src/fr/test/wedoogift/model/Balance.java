package fr.test.wedoogift.model;

public class Balance {
	private int wallet_id;
	private float amount;
	
	public Balance(int wallet_id, float amount) {
		this.wallet_id = wallet_id;
		this.amount = amount;
	}
	
	public Balance() {}

	public int getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Balance [wallet_id=" + wallet_id + ", amount=" + amount + "]";
	}
}

package fr.test.wedoogift.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Output {
	private List<Company> companies;
	private List<User> users;
	private List<Distribution> distributions;
	
	public Output(List<Company> companies, List<User> users, List<Distribution> distributions) {
		this.companies = companies;
		this.distributions = distributions;
		this.users = users;
	}
	
	public Output(List<Company> companies, List<User> users) {
		this.companies = companies;
		this.users = users;
	}
	
	public Output() {}

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Distribution> getDistributions() {
		return distributions;
	}

	public void setDistributions(List<Distribution> distributions) {
		this.distributions = distributions;
	}
	
	public Output distribute(List<User> users, List<Company> companies) throws ParseException {
		List<Distribution> distributions = new ArrayList<>();
		Company company = new Company();
		int i = 0;
		for(User u : users) {
			for(Balance b : u.getBalanceList()) {
				if(b.getWallet_id() == 1) {
					i++;
					distributions.add(company.distributeGiftCard(u, b.getAmount(), i, b.getWallet_id()));
				}else if(b.getWallet_id() == 2) {
					i++;
					distributions.add(company.distributeMealVoucher(u, b.getAmount(), i, b.getWallet_id()));
				}
			}
		}
		Output output = new Output(companies, users, distributions);
		return output;
	}
	@Override
	public String toString() {
		return "Output [companies=" + companies + ", users=" + users + ", distributions=" + distributions + "]";
	}
	
	
}

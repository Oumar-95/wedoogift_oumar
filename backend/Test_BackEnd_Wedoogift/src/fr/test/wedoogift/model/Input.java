package fr.test.wedoogift.model;

import java.util.Arrays;

public class Input {
	private Company [] companies;
	private User [] users;
	private Distribution [] distributions;
	private Wallet [] wallets;
	
	public Input(Company[] companies, User[] users, Distribution[] distributions, Wallet [] wallets) {
		this.companies = companies;
		this.users = users;
		this.distributions = distributions;
		this.wallets = wallets;
	}

	@Override
	public String toString() {
		return "Input [companies=" + Arrays.toString(companies) + ", users=" + Arrays.toString(users)
				+ ", distributions=" + Arrays.toString(distributions) + ", wallets=" + Arrays.toString(wallets) + "]";
	}

	
}

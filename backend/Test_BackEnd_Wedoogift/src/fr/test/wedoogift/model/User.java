package fr.test.wedoogift.model;
import java.util.List;

public class User {
	private int user_id;
	private List<Balance> balanceList;
	
	public User(int user_id, List<Balance> balanceList) {
		this.user_id = user_id;
		this.balanceList = balanceList;
	}
	public User() {}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public List<Balance> getBalanceList() {
		return balanceList;
	}
	public void setBalanceList(List<Balance> balanceList) {
		this.balanceList = balanceList;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", balanceList=" + balanceList + "]";
	}
}

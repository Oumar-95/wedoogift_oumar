package fr.test.wedoogift.model;

public class Distribution {
	private int id;
	private float amount;
	private String start_date;
	private String end_date;
	private int company_id;
	private int user_id;
	private int wallet_id;
	
	public Distribution(int id, float amount, String start_date, String end_date, int company_id, int user_id, int wallet_id){
		this.amount = amount;
		this.company_id = company_id;
		this.end_date = end_date;
		this.start_date = start_date;
		this.id = id;
		this.user_id = user_id;
		this.wallet_id = wallet_id;
	}
	public Distribution() {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public int getWallet_id() {
		return wallet_id;
	}
	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}
	@Override
	public String toString() {
		return "Distribution [id=" + id + ", amount=" + amount + ", start_date=" + start_date + ", end_date=" + end_date
				+ ", company_id=" + company_id + ", user_id=" + user_id + ", wallet_id=" + wallet_id + "]";
	}
	
	
}

package fr.test.wedoogift.model;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date; 
public class Company {
	private int company_id;
	private String company_name;
	private float balance;
	public Company(int id, String company_name, float balance) {
		this.company_id = id;
		this.company_name = company_name;
		this.balance = balance;
	}
	public Company() {}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	
	public Distribution distributeGiftCard(User user, float nbGiftCard, int id, int wallet_id) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
	    Date date = new Date();
	    String date_start = formatter.format(date);
	    Calendar c = Calendar.getInstance();
	    c.setTime(formatter.parse(date_start));
	    c.add(Calendar.DATE, 365);
	    String date_end = formatter.format(c.getTime()); 
	    int company_id = this.company_id;
	    int user_id = user.getUser_id();
		Distribution distribution = new Distribution(id, nbGiftCard, date_start, date_end, company_id, user_id, wallet_id);
		if(nbGiftCard <= balance) {
			this.balance = this.balance - nbGiftCard;
		}
		return distribution;
	}
	
	public Distribution distributeMealVoucher(User user, float nbGiftCard, int id, int wallet_id) throws ParseException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
	    Date date = new Date();
	    String date_start = formatter.format(date);
	    Calendar c = Calendar.getInstance();
	    c.setTime(formatter.parse(date_start));
	    c.add(Calendar.MONTH, 2);
	    String date_end = formatter.format(c.getTime()); 
	    int company_id = this.company_id;
	    int user_id = user.getUser_id();
		Distribution distribution = new Distribution(id, nbGiftCard, date_start, date_end, company_id, user_id, wallet_id);
		if(nbGiftCard <= balance) {
			this.balance = this.balance - nbGiftCard;
		}
		return distribution;
	}
	
	@Override
	public String toString() {
		return "Company [company_id=" + company_id + ", company_name=" + company_name + ", balance=" + balance + "]";
	}
}

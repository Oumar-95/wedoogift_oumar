package fr.test.wedoogift.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import fr.test.wedoogift.model.Balance;
import fr.test.wedoogift.model.Company;
import fr.test.wedoogift.model.Output;
import fr.test.wedoogift.model.User;
import fr.test.wedoogift.model.Wallet;


public class TestBackEndMain {

	public static void main(String[] args) throws ParseException {
		Company company1 = new Company(1, "Wedoogift", 1000);
		Company company2 = new Company(2, "Wedoofood", 3000);
		List<Company> companies = new ArrayList<Company>();
		companies.add(company1);
		companies.add(company2);
		
		Wallet wallet1 = new Wallet(company1.getCompany_id(), "gift cards", "GIFT");
		Wallet wallet2 = new Wallet(company2.getCompany_id(), "food cards", "FOOD");
		
		Balance balance1 = new Balance(wallet1.getId(), 100);
		Balance balance2 = new Balance(wallet2.getId(), 250);
		Balance balance3 = new Balance(wallet1.getId(), 100);
		Balance balance4 = new Balance(wallet1.getId(), 1000);
		
		List<Balance> balanceList1 = new ArrayList<>();
		balanceList1.add(balance1);
		balanceList1.add(balance2);
		
		List<Balance> balanceList2 = new ArrayList<>();
		balanceList2.add(balance3);
		
		List<Balance> balanceList3 = new ArrayList<>();
		balanceList3.add(balance4);
		
		User user1 = new User(1, balanceList1);
		User user2 = new User(2, balanceList2);
		User user3 = new User(3, balanceList3);
		List<User> userList = new ArrayList<User>();
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		
		Output output = new Output();
		System.out.println(output.distribute(userList, companies));
	}
}
